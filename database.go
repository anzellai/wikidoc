package main

import (
	"sort"
	"sync"
	"time"
)

// Database is a struct and dump database holding Wiki records by title as map key
type Database struct {
	Wiki map[string]Documents
	lock *sync.RWMutex
}

// NewDatabase returns a db instance
func NewDatabase() *Database {
	return &Database{
		Wiki: map[string]Documents{},
		lock: &sync.RWMutex{},
	}
}

func (db *Database) createOrUpdate(title, content string) (*Document, error) {
	// create a new revision
	doc := &Document{
		Title:     title,
		Content:   content,
		Timestamp: time.Now().Unix(),
		Patch:     NewPatch("", content),
	}
	err := doc.validate()
	if err != nil {
		return nil, err
	}

	existed, err := db.getByTitle(title)
	if err == nil {
		doc.Patch = NewPatch(existed[0].Patch.to, doc.Content)
	}
	doc.Content = ""

	db.lock.Lock()
	defer db.lock.Unlock()

	db.Wiki[title] = append(db.Wiki[title], doc)
	// sort timestamp in descending order
	sort.Sort(db.Wiki[title])

	return doc, nil
}

func (db *Database) getByTitle(title string) (Documents, error) {
	db.lock.RLock()
	defer db.lock.RUnlock()

	docs, ok := db.Wiki[title]
	if !ok {
		return nil, errTitleNotFound
	}

	results := Documents{}
	for _, doc := range docs {
		result := &Document{
			Title:     doc.Title,
			Content:   doc.Patch.Apply(doc.Patch.from),
			Timestamp: doc.Timestamp,
		}
		results = append(results, result)
	}

	return results, nil
}

func (db *Database) getByTimestamp(title string, timestamp int64) (*Document, error) {
	db.lock.RLock()
	defer db.lock.RUnlock()

	docs, err := db.getByTitle(title)
	if err != nil {
		return nil, err
	}

	for _, doc := range docs {
		if doc.Timestamp >= timestamp {
			return doc, nil
		}
	}

	return nil, errRevisionNotFound
}
