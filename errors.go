package main

import "errors"

// defining errors aligned with project constraints
var (
	errContentTypeNotAccepted = errors.New("content-type not accepted, please use application/json")
	errRouteNotFound          = errors.New("route not found")
	errTitleNotFound          = errors.New("title not found")
	errTitleExceedingLimit    = errors.New("title cannot exceed 50 characters long")
	errRevisionNotFound       = errors.New("revision not found")
	errInvalidContent         = errors.New("invalid content")
	errInvalidTitle           = errors.New("invalid title")
	errInvalidTimestamp       = errors.New("invalid timestamp")
)

// APIError struct for rendering any API error response
type APIError struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
}
