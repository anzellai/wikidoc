package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

// App struct holding app handles
type App struct {
	db     *Database
	logger *log.Logger
}

func (app *App) Run() {
	srv := &http.Server{
		Handler:      app.NewRouter(),
		Addr:         "127.0.0.1:8000",
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  5 * time.Second,
	}

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// We received an interrupt signal, shut down.
		if err := srv.Shutdown(context.Background()); err != nil {
			// listener error or context timeout
			app.logger.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	app.logger.Println("HTTP Server starting...")

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		app.logger.Printf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
}

// LoggingMiddleware will log all incoming requests
func (app *App) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		app.logger.Printf("[%s] %s", r.Method, r.URL.Path)
		next.ServeHTTP(w, r)
	})
}

// NewRouter returns a new Document resource router
func (app *App) NewRouter() *mux.Router {
	router := mux.NewRouter()
	router.Use(app.LoggingMiddleware)

	api := router.PathPrefix("/documents").Subrouter()
	api.HandleFunc("", app.TitlesHandler).
		Methods("GET")
	api.HandleFunc("/{title:[a-zA-Z0-9-]+}", app.TitleHandler).
		Methods("GET", "POST")
	api.HandleFunc("/{title:[a-zA-Z0-9-]+}/latest", app.LatestTitleHandler).
		Methods("GET")
	api.HandleFunc("/{title:[a-zA-Z0-9-]+}/{timestamp:[0-9]+}", app.TitleByTimestampHandler).
		Methods("GET")

	router.NotFoundHandler = http.HandlerFunc(app.notFoundHandler)

	return router
}

// notFoundHandler handles rendering 404 response for routes not matching
func (app *App) notFoundHandler(w http.ResponseWriter, r *http.Request) {
	app.renderJSON(w, 404, errRouteNotFound, nil)
}

// renderJSON is a helper function to render JSON response
func (app *App) renderJSON(w http.ResponseWriter, code int, err error, doc interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if code > 299 {
		apiError := &APIError{Code: code, Error: err.Error()}
		response, err := json.Marshal(apiError)
		if err != nil {
			// this shouldn't happen, just in case it does, make it critical
			app.logger.Fatalf("Critical error while rendering response: %+v", err)
			w.WriteHeader(500)
			return
		}
		w.WriteHeader(code)
		w.Write(response)
		return
	}

	response, err := json.Marshal(doc)
	if err != nil {
		// this shouldn't happen, just in case it does, make it critical
		app.logger.Fatalf("Critical error while rendering response: %+v", err)
		w.WriteHeader(500)
		return
	}

	w.WriteHeader(code)
	w.Write(response)
}

// TitlesHandler handles rendering all available titles stored in Database
func (app *App) TitlesHandler(w http.ResponseWriter, r *http.Request) {
	titles := []string{}
	for title := range app.db.Wiki {
		titles = append(titles, title)
	}

	app.renderJSON(w, 200, nil, titles)
}

// TitleHandler handles rendering and updating a particular title stored in Database
func (app *App) TitleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	title := vars["title"]

	// POST to create an updated revision of title
	if r.Method == "POST" {
		docUser := &Document{}
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&docUser)
		if err != nil {
			app.renderJSON(w, 400, errInvalidContent, nil)
			return
		}
		defer r.Body.Close()

		doc, err := app.db.createOrUpdate(title, docUser.Content)
		if err != nil {
			app.renderJSON(w, 400, err, nil)
			return
		}
		doc = applyPatch(doc)
		app.renderJSON(w, 200, nil, doc)
		return
	}

	docs, err := app.db.getByTitle(title)
	if err != nil {
		app.renderJSON(w, 404, err, nil)
		return
	}

	app.renderJSON(w, 200, nil, docs)
}

// LatestTitleHandler renders the latest revision of document
func (app *App) LatestTitleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	title := vars["title"]

	docs, err := app.db.getByTitle(title)
	if err != nil {
		app.renderJSON(w, 404, err, nil)
		return
	}

	app.renderJSON(w, 200, nil, docs[0])
}

// TitleByTimestampHandler renders the revision of document by title and timestamp
func (app *App) TitleByTimestampHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	title := vars["title"]
	timestamp, err := strconv.ParseInt(vars["timestamp"], 10, 64)
	if err != nil {
		app.renderJSON(w, 400, errInvalidTimestamp, nil)
		return
	}

	doc, err := app.db.getByTimestamp(title, timestamp)
	if err != nil {
		app.renderJSON(w, 404, errRevisionNotFound, nil)
		return
	}

	app.renderJSON(w, 200, nil, doc)
}
