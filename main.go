package main

import (
	"log"
	"os"
)

// app as the global pointer to access db and logger
var app *App

// main entry point
func main() {
	app = &App{
		db:     NewDatabase(),
		logger: log.New(os.Stdout, "WikiDoc [HTTP] ", log.LstdFlags),
	}

	app.Run()
}
