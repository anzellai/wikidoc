package main

type Patch struct {

	// Implementation detail, should not be accessed

	from string

	to string
}

func NewPatch(from string, to string) *Patch {

	return &Patch{from, to}

}

func (patch *Patch) Apply(from string) string {

	if patch.from != from {

		panic("Patch applied incorrectly")

	}

	return patch.to

}
