package main

// Document struct defines all fields for SimpleWiki Document
type Document struct {
	Title     string `json:"title"`
	Content   string `json:"content"`
	Timestamp int64  `json:"timestamp"`
	Patch     *Patch `json:"-"`
}

// Documents type for document collections
type Documents []*Document

// implement Sort interface for sorting with timestamp in decending order
func (docs Documents) Len() int           { return len(docs) }
func (docs Documents) Swap(i, j int)      { docs[i], docs[j] = docs[j], docs[i] }
func (docs Documents) Less(i, j int) bool { return docs[i].Timestamp > docs[j].Timestamp }

func (doc *Document) validate() error {
	if doc.Title == "" {
		return errInvalidTitle
	}
	if len(doc.Title) > 50 {
		return errTitleExceedingLimit
	}
	if doc.Content == "" {
		return errInvalidContent
	}

	return nil
}

func applyPatch(doc *Document) *Document {
	if doc.Patch == nil {
		return doc
	}
	applied := &Document{
		Title:     doc.Title,
		Content:   doc.Patch.Apply(doc.Patch.from),
		Timestamp: doc.Timestamp,
	}
	return applied
}
