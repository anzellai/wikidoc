package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

var (
	testApp  *App
	recorder *httptest.ResponseRecorder
)

func testParseJSON(t *testing.T, body *bytes.Buffer, dst interface{}) {
	err := json.NewDecoder(body).Decode(&dst)
	if err != nil {
		t.Fatal("cannot parse JSON response body, error ", err.Error())
	}
}

func testWiki() Documents {
	return Documents{
		&Document{
			Title:     "test-123",
			Content:   "",
			Timestamp: 987654321,
			Patch:     NewPatch("this is test 1", "this is test 1 revised"),
		},
		&Document{
			Title:     "test-123",
			Content:   "",
			Timestamp: 123456789,
			Patch:     NewPatch("", "this is test 1"),
		},
	}
}

func setup() {
	testApp = &App{
		db:     NewDatabase(),
		logger: log.New(os.Stdout, "[Test] WikiDoc [HTTP] ", log.LstdFlags),
	}
	testApp.db.Wiki["test-123"] = testWiki()
	recorder = httptest.NewRecorder()
}

func testGet(t *testing.T, path string, expected int) {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		t.Fatal("cannot create new request!")
	}

	router := testApp.NewRouter()
	router.ServeHTTP(recorder, req)
	if recorder.Code != expected {
		t.Fatal("expected ", expected, " but it is", recorder.Code)
	}
}

func testPost(t *testing.T, path string, body interface{}, expected int) {
	bodyJSON, err := json.Marshal(body)
	if err != nil {
		t.Fatal("cannot marshal body to JSON, error ", err.Error())
	}

	req, err := http.NewRequest("POST", path, bytes.NewBuffer(bodyJSON))
	if err != nil {
		t.Fatal("cannot create new request!")
	}

	router := testApp.NewRouter()
	router.ServeHTTP(recorder, req)
	if recorder.Code != expected {
		t.Fatal("expected ", expected, " but it is", recorder.Code)
	}
}

func TestNotFound(t *testing.T) {
	setup()
	testGet(t, "/404", 404)
}

func TestGetLatest(t *testing.T) {
	setup()
	testGet(t, "/documents/test-123/latest", 200)

	result := &Document{}
	testParseJSON(t, recorder.Body, result)
	if !reflect.DeepEqual(result, applyPatch(testWiki()[0])) {
		t.Fatal("expected response ", testWiki()[0], ", got ", result)
	}
}

func TestGetTitles(t *testing.T) {
	setup()
	testGet(t, "/documents", 200)

	result := &[]string{}
	testParseJSON(t, recorder.Body, result)
	if !reflect.DeepEqual(result, &[]string{"test-123"}) {
		t.Fatal("expected ", []string{"test-123"}, ", got ", result)
	}
}

func TestCreateTitle(t *testing.T) {
	setup()
	testDoc := &Document{Content: "this is test doc"}
	testPost(t, "/documents/test-doc", testDoc, 200)

	result := &Document{}
	testParseJSON(t, recorder.Body, result)
	if result.Title != "test-doc" {
		t.Fatal("expected title test-doc, got ", result.Title)
	}
	if result.Content != "this is test doc" {
		t.Fatal("expected title 'this is test doc', got ", result.Content)
	}
	if result.Timestamp == 0 {
		t.Fatal("expected timestamp creation, got '0'")
	}
}

func TestCreateTitleExceedingLimit(t *testing.T) {
	setup()
	testDoc := &Document{Content: "this is test doc"}
	testPost(t, "/documents/this-is-test-doc-just-exceeding-the-50-characters-limit", testDoc, 400)

	result := &APIError{}
	testParseJSON(t, recorder.Body, result)
	if result.Error != errTitleExceedingLimit.Error() {
		t.Fatal("excpected error ", errTitleExceedingLimit.Error(), ", got error ", result.Error)
	}
}

func TestCreateTitleInvalidContent(t *testing.T) {
	setup()
	testDoc := &Document{Content: ""}
	testPost(t, "/documents/test-doc", testDoc, 400)

	result := &APIError{}
	testParseJSON(t, recorder.Body, result)
	if result.Error != errInvalidContent.Error() {
		t.Fatal("excpected error ", errInvalidContent.Error(), ", got error ", result.Error)
	}
}

func TestGetTitle(t *testing.T) {
	setup()
	testGet(t, "/documents/test-123", 200)

	result := Documents{}
	testParseJSON(t, recorder.Body, &result)
	testApplied := Documents{}
	for _, doc := range testWiki() {
		testApplied = append(testApplied, applyPatch(doc))
	}
	if !reflect.DeepEqual(result, testApplied) {
		t.Fatal("expected response ", testWiki()[0], ", got ", result)
	}

}

func TestGetByTimestamp(t *testing.T) {
	setup()
	testDoc := &Document{Content: "this is test doc"}
	testPost(t, "/documents/test-doc", testDoc, 200)
	testDoc.Title = "test-doc"

	result := &Document{}
	testParseJSON(t, recorder.Body, &result)

	if result.Title != testDoc.Title {
		t.Fatal("expected title ", testDoc.Title, ", got ", result.Title)
	}
	if result.Content != testDoc.Content {
		t.Fatal("expected content ", testDoc.Content, ", got ", result.Content)
	}
	if result.Timestamp == 0 {
		t.Fatal("expected auto timestamp but got 0")
	}

	testGet(t, fmt.Sprintf("/documents/test-doc/%d", result.Timestamp), 200)
	testGet(t, fmt.Sprintf("/documents/test-doc/%d", result.Timestamp+1), 200)
}
